#!/usr/bin/env python
# Description:
# This is the library for the implementation of Unscented Kalman-Consensus Filter.
# Authored by:
# Hilton Tnunay
# PhD Candidate in Control System at The University of Manchester, UK
# Email: hilton.tnunay@manchester.ac.uk

import sys
import math
import numpy as np
from scipy.linalg import sqrtm
from scipy.linalg import cholesky

"""Class Declaration
Name: Network
Usage: Defining the properties of a network
Note: define the process and measurement functions here
"""
class Network:
    def __init__(self, inNumberOfAgent, inAdjacency, inF, inH):
        """Function Description
        Input:
        # <inNumberOfAgent> : scalar
          The number of agent within a network.
        # <inAdjacency> : matrix
          A matrix that models the connectivity of a network.
        # <inF> : matrix or vector of functions
          A vector or matrix of process dynamics. Set to <None> if using the default.
        # <inH> : matrix or vector of functions. Set to <None> if using the default.
          A vector or matrix of measurement dynamics
        """
        self.numberOfAgent = inNumberOfAgent
        self.adjacency = inAdjacency
        self.F = inF
        self.H = inH
    def processFunc(self, inState, inProcessNoise):
        """Function Description
        # <inState> : vector
          Vector of current state
        # <inProcessNoise> : vector
          Vector of process noise
        """
        if self.F is None:
            # Define the process function here, such that x_k+1 = f(x_k, w_k)
            self.nextState = self.F*inState + inProcessNoise
        else:
            # This condition is satisfied if the process matrix is defined, such that
            # x_k+1 = f(x_k, w_k) can be simplified to be x_k+1 = F*x_k + w_k
            self.nextState = self.F*inState + inProcessNoise
    def measureFunc(self, inState, inMeasureNoise):
        """Function Description
        # <inState> : vector
          Vector of current state
        # <inMeasureNoise> : vector
          Vector of measurement noise
        """
        if self.H is None:
            # Define the measurement function here, such that y_k = h(x_k, v_k)
            self.measurement = self.H*inState + inMeasureNoise
        else:
            # This condition is satisfied if the measurement matrix is defined, such that
            # y_k = h(x_k, u_k) can be simplified to be y_k = H*x_k + v_k
            self.measurement = self.H*inState + inMeasureNoise

"""Class Declaration
Name: Agent
Usage: Defining the parameters and methods of an agent
"""
class Agent:    
    def __init__(self, inUpperBoundOfNextState, inLowerBoundOfNextState):
        """Function Description
        # <inUpperBoundOfNextState> : vector
          A vector containing the upper boundaries of the next state
        # <inLowerBoundOfNextState> : vector
          A vector containing the lower boundaries of the next state
        """               
        self.upperBoundOfNextState = inUpperBoundOfNextState
        self.lowerBoundOfNextState = inLowerBoundOfNextState
    def setState(self, inState):
        """Function Description
        # <inState> : vector
          Vector of current state
        """  
        self.state = inState
    def setCurrentMeasure(self, inCurrMeasure):
        """Function Description
        # <inCurrMeasure> : vector
          Vector of current measurement
        """
        self.measurement = inCurrMeasure
    def setNextState(self, inNextState):
        """Function Description
        # <inNextState> : vector
          Vector of next state
        """        
        self.nextState = np.maximum(inNextState, self.upperBoundOfNextState)
        self.nextState = np.minimum(inNextState, self.lowerBoundOfNextState)    

"""Class Declaration
Name: UnscentedTransform
"""
class UnscentedTransform:
    def __init__(self, inNumberOfState, inNumberOfMeasurement,
                 inVarRho1, inVarRho2):
        self.numberOfState = inNumberOfState
        self.numberOfMeasurement = inNumberOfMeasurement                 
        self.paramVarRho1 = inVarRho1
        self.paramVarRho2 = inVarRho2
        self.paramR = 2*self.numberOfState + self.numberOfMeasurement
        self.paramLambda = math.pow(self.paramVarRho1,2)*(self.paramR + self.paramVarRho2) - self.paramR
        self.paramVarPhi = self.paramR + self.paramLambda
    def updateSigmaPoints(self, inMean, inCovariance):
        dimCov = np.shape(inCovariance)
        self.paramR = dimCov[0]
        onesR = np.ones(self.paramR)
        augMean = np.kron(onesR, inMean)
        sqrtCovariance = sqrtm(self.paramVarPhi*inCovariance)
        sigmaPointsPlus = augMean + sqrtCovariance
        sigmaPointsMin = augMean - sqrtCovariance
        self.sigmaPoints = np.concatenate((inMean, sigmaPointsPlus, sigmaPointsMin), axis=1)
    def updateWeight(self):
        self.weightMean = np.full(2*self.paramR + 1, 0.5/self.paramVarPhi)
        self.weightCov = np.full(2*self.paramR + 1, 0.5/self.paramVarPhi)
        self.weightMean[0] = self.paramLambda/self.paramVarPhi
        self.weightCov[0] = self.paramLambda/self.paramVarPhi + (1 - math.pow(self.paramVarRho1,2) + self.paramVarRho2)
    def updatePriorSigmaPts(self, dynFunc, inSigmaPts1, inSigmaPts2):
        return dynFunc(inSigmaPts1, inSigmaPts2)
    def updatePriorMean(self, inSigmaMean):
        dimIn = np.shape(inSigmaMean)
        sumMean = np.zeros((dimIn[0], 1))
        for l in range(2*self.paramR+1):
            sumMean += self.weightMean[l]*inSigmaMean[:,[l]]
        return sumMean
    def updatePriorJointCovariance(self, inSigmaMean_i, inSigmaMean_j, inPriorMean_i, inPriorMean_j):
        dim_i = np.shape(inPriorMean_i)
        dim_j = np.shape(inPriorMean_j)
        sumCovariance = np.zeros((dim_i[0], dim_j[0]))
        for l in range(2*self.paramR+1):
            tempCov_i = (inSigmaMean_i[:,[l]] - inPriorMean_i)
            tempCov_j = (inSigmaMean_j[:,[l]] - inPriorMean_j)
            sumCovariance += self.weightCov[l]*np.matmul(tempCov_i, tempCov_j.transpose())
        return sumCovariance

"""Class Declaration
Name: KalmanConsensusFilter
Usage: 
1. Defining the properties of an agent
2. Executing the KCF-related methods
"""
class KalmanConsensusFilter:
    def __init__(self, inAdjacency, 
                 inStateCovariance, inProcessNoiseCovariance, inMeasureNoiseCovariance,
                 inState, inMeasurement, inProcessNoise, inMeasurementNoise,
                 inVarRho1, inVarRho2, inDt):
        self.stateCovariance = inStateCovariance
        self.processNoiseCovariance = inProcessNoiseCovariance
        self.measureNoiseCovariance = inMeasureNoiseCovariance        

        self.adjacency = inAdjacency

        self.state = inState
        self.measurement = inMeasurement
        self.processNoise = inProcessNoise
        self.measurementNoise = inMeasurementNoise
        
        self.KalmanGain = None
        self.consensusGain = None
        self.dt = inDt

        dimState = np.shape(inState)
        dimMeasure = np.shape(inMeasurement)
        
        self.UT = UnscentedTransform(dimState[0], dimMeasure[0], inVarRho1, inVarRho2)
    def predictionStep(self):
        self.augState = np.concatenate((self.state, self.processNoise, self.measurementNoise))
        
        dimP = np.shape(self.stateCovariance)
        dimW = np.shape(self.processNoiseCovariance)
        dimV = np.shape(self.measureNoiseCovariance)
        temp_1 = np.concatenate((self.stateCovariance, np.zeros((dimP[0], dimW[1])), np.zeros((dimP[0], dimV[1]))))
        temp_2 = np.concatenate((np.zeros((dimW[0], dimP[1])), self.processNoiseCovariance, np.zeros((dimW[0], dimV[1]))))
        temp_3 = np.concatenate((np.zeros((dimV[0], dimP[1])), np.zeros((dimV[0], dimW[1])), self.measureNoiseCovariance))
        self.augCovariance = np.concatenate((temp_1, temp_2, temp_3), axis=0)
        self.sigmaPoints = self.UT.updateSigmaPoints(self.augState, self.augCovariance)

class localNetwork:
    def __init__(self, inNumberOfOutput, inNumberOfMeasurement, inC):
        self.numberOfOutput = inNumberOfOutput
    def updateMeasurement(self, inReading):
        self.reading = inReading
    def calculateNextState(self, inInput, inState):
        self.input = inInput
        self.state = inState
    def calculateInput(self, inState):
        self.state = inState

def updateTopology(initFlag, inNumberOfAgent, inNumberOfState):    
	global Adjacency
	global Laplacian
	global State
	global Offset
	global Command

	global numberOfAgent
	global numberOfState

	numberOfAgent = inNumberOfAgent
	numberOfState = inNumberOfState
	Adjacency = np.zeros((numberOfAgent, numberOfAgent))
	Laplacian = np.zeros((numberOfAgent, numberOfAgent))
	State = np.zeros((numberOfAgent*numberOfState,1))
	Offset = np.zeros((numberOfAgent*numberOfState,1))
	Command = np.zeros((numberOfAgent*numberOfState,1))
	# return Adjacency
