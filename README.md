# Testbed of Unscented Kalman-Consensus Filter

## Description
Platform for implementing distributed algorithm of Unscented Kalman-Consensus Filter.

## Author

Hilton Tnunay, PhD Candidate in Control Systems at The University of Manchester (Email: hilton.tnunay@manchester.ac.uk).